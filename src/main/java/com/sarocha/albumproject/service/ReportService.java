/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sarocha.albumproject.service;

import com.sarocha.albumproject.dao.SaleDao;
import com.sarocha.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author Sarocha
 */
public class ReportService {

    public List<ReportSale> getReportSaleByDay() {
        SaleDao dao = new SaleDao();
        return dao.getDayReportSale();
    }

    public List<ReportSale> getReportSaleByMonth(int year) {
        SaleDao dao = new SaleDao();
        return dao.getMonthReportSale(year);
    }

}
